#!/usr/bin/env python
import csv
import datetime
import json
import os  #charge librairie manip fichiers
import sys

import pygit2

dir = 'data-republique-numerique'
repository_path = pygit2.discover_repository(dir)
repository = pygit2.Repository(repository_path)
author_slug_by_opinion_id = {}
title_slug_by_opinion_id = {}
vote_by_date_by_opinion_id = {}
for commit in repository.walk(repository.head.target, pygit2.GIT_SORT_REVERSE):
    if commit.author.name != 'FTABT Bot':
        continue
    date = datetime.datetime.fromtimestamp(commit.commit_time)
    tree = commit.tree
    for tree_entry in tree:
        if tree_entry.name.startswith('data-') and tree_entry.name.endswith('.json'):
            blob = repository.get(tree_entry.id)
            data = json.loads(blob.data)
            opinion = data['opinion']
            author_slug_by_opinion_id[opinion['id']] = opinion['author']['_links']['profile'].split('/')[-1]
            title_slug_by_opinion_id[opinion['id']] = opinion['_links']['show'].split('/')[-1]
            vote = {
                "mitige": opinion['votes_mitige'],
                "nok": opinion['votes_nok'],
                "ok": opinion['votes_ok'],
                }
            vote_by_date = vote_by_date_by_opinion_id.get(opinion['id'])
            if vote_by_date is None:
                vote_by_date_by_opinion_id[opinion['id']] = vote_by_date = {}
            vote_by_date[date] = vote
if not os.path.exists('csv'):
    os.mkdir('csv')
for opinion_id, vote_by_date in vote_by_date_by_opinion_id.items():
    author_slug = author_slug_by_opinion_id[opinion_id]
    author_dir = 'csv/{}'.format(author_slug)
    if not os.path.exists(author_dir):
        os.mkdir(author_dir)
    with open('{}/{}-{}.csv'.format(author_dir, opinion_id, title_slug_by_opinion_id[opinion_id]), 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(['Date', 'Pour', 'Contre', 'Mitige'])
        ordered_list = []
        for date, vote in vote_by_date.items():
            ordered_list.append((date, vote))
        ordered_list.sort(key = lambda date_vote_couple: date_vote_couple[0])
        for date, vote in ordered_list:
            writer.writerow([date.isoformat(), vote['ok'], vote['nok'], vote['mitige']])
