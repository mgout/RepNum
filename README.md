#Chronologie des votes Republique Numerique

Le script extract.py extrait les données présentes dans  https://git.framasoft.org/etalab/data-republique-numerique et les trie dans des csv (par rep auteur), affiche l'historique des votes de chaque contribution (compte des votes : pour, contre, mitigé)
